import datetime
import random


class DataManager:

    def select_period(self, start, date: list, data: list, horizon: int = 24,name="Production"):
        """Method to select specified periods from given data set"""
        end = start + datetime.timedelta(hours=horizon)
        print(f"SELECTING {name} DATA BETWEEN: {start} AND {end}")
        if end > date[-1]:
            end = end - datetime.timedelta(hours=1)
            if end > date[-1]:
                end = date[-1]

        for unit in date:
            if unit == start:
                select_periodion = []
                d = []
                debut = date.index(unit)
                fin = date.index(end)

                for i in range(debut, fin):
                    select_periodion.append(data[i])
                    d.append(date[i])

                break

        return end, select_periodion, d

    def correction(self,df):
        """Methood to correct errors in charging data measurements"""
        indx = df.index
        for i in range(len(indx) - 1):
            for y in range(1, 3):
                hold = df[f"charger {y}"][indx[i]]

                #             print (hold)

                if hold > 14 and hold <= 21:
                    df[f"charger {y}"][indx[i]] = 0
                    for x in range(2, -1, -1):

                        if hold > 7:
                            val = random.uniform(6, 7)

                        else:
                            val = hold

                        hold -= val
                        df[f"charger {y}"][indx[i - x]] += val
                #

                elif hold > 21:
                    df[f"charger {y}"][indx[i]] = 0
                    for x in range(2, -1, -1):

                        if hold - 14 > 0:
                            val = random.uniform(10.6, 13.94)

                        else:
                            val = hold

                        hold -= val
                        df[f"charger {y}"][indx[i - x]] += val

                #
                else:
                    pass

        return df

    def restructure(self, df):
        d = []
        for index, frame in df.groupby([(df.index.month), (df.index.day)]):
            d.append(frame)
        return  d